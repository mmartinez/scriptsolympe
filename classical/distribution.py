import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.classical import *

from pythonplot.latex import *
from pythonplot.movie import *

from matplotlib import cm

mode=sys.argv[1]
wdir=sys.argv[2]


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	tmax=int(params['tmax'])
	ntraj=int(params['ntraj'])
	T0=float(params['T0'])
	ndt=int(params['ndt'])
	gamma=float(params['gamma'])
	e=float(params['epsilon'])

	dx=0.1
	dp=0.1
	
	pot=ModulatedPendulum(e,gamma)
	tp=ContinuousTimePropagator(pot,T0=T0,ndt=ndt)

	x,p=np.zeros((ntraj,tmax)),np.zeros((ntraj,tmax))


	for itraj in range(ntraj):
		x[itraj,0]=np.pi+(np.random.rand()-0.5)*dx
		p[itraj,0]=0.0+(np.random.rand()-0.5)*dp
		t=np.zeros(tmax)
		for it in range(tmax-1):
			y=np.array([x[itraj,it],p[itraj,it]])
			y,t[it+1]=tp.propagate(y,t[it])
			x[itraj,it+1],p[itraj,it+1]=y

	
	np.savez(wdir+"dataruns/"+str(runid),"w", x=x,p=p)

	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	nruns=int(params['nruns'])
	ntraj=int(params['ntraj'])
	tmax=int(params['tmax'])
	
	x=np.zeros((nruns*ntraj,tmax))
	p=np.zeros((nruns*ntraj,tmax))
	t=np.arange(tmax)

	for i in range(nruns):
		data=np.load(wdir+"dataruns/"+str(i)+".npz")
		x[i*ntraj:(i+1)*ntraj]=data['x']
		p[i*ntraj:(i+1)*ntraj]= data['p']
		data.close()
			
	np.savez(wdir+"data","w", x=x, p=p,t=t)
	os.system("rm -r "+wdir+"dataruns/")


	
