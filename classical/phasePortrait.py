import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.classical import *

from pythonplot.latex import *
from pythonplot.movie import *

from matplotlib import cm

mode=sys.argv[1]
wdir=sys.argv[2]



if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	potential=params['potential']
	tmax=int(params['tmax'])
	nruns=int(params['nruns'])
	
	
	kicked=False
	
	if potential=="DoubleWell":
		T0=float(params['T0'])
		ndt=int(params['ndt'])
		icg=InitialConditionGenerator(nruns)
		gamma=float(params['gamma'])
		e=float(params['epsilon'])
		pot=DoubleWell(e,gamma)
		tp=ContinuousTimePropagator(pot,T0=T0,ndt=ndt)
		pp=PhasePortrait(tmax,nruns,tp,icg.generateXP)
		
	if potential=="DoubleWellKicked":
		icg=InitialConditionGenerator(nruns)
		gamma=float(params['gamma'])
		pot=DoubleWellKicked(gamma)
		tp=tp=DiscreteTimePropagator(pot)
		pp=PhasePortrait(tmax,nruns,tp,icg.generateX,kicked=True)
		kicked=True
	
	if potential=="ModulatedPendulum" or potential=="ModulatedPendulumRotation":
		T0=float(params['T0'])
		ndt=int(params['ndt'])
		icg=InitialConditionGenerator(nruns)
		gamma=float(params['gamma'])
		e=float(params['epsilon'])
		phi=float(params['phi'])
		pot=ModulatedPendulum(e,gamma,phi=phi)
		tp=ContinuousTimePropagator(pot,T0=T0,ndt=ndt)
		pp=PhasePortrait(tmax,nruns,tp,icg.generateXP)
		# ~ pp=PhasePortrait(tmax,nruns,tp,icg.generateP)
		
	if potential=="ModulatedPendulumFuku":
		T0=float(params['T0'])
		ndt=int(params['ndt'])
		icg=InitialConditionGenerator(nruns)
		gamma=float(params['gamma'])
		e=float(params['epsilon'])
		phi=float(params['phi'])
		pot=ModulatedPendulum(e,gamma,mw=np.sin,phi=phi)
		tp=ContinuousTimePropagator(pot,T0=T0,ndt=ndt)
		pp=PhasePortrait(tmax,nruns,tp,icg.generateXP)
		# ~ pp=PhasePortrait(tmax,nruns,tp,icg.generateP)
		
		
	if potential=="ModulatedPendulumTrig":
		T0=float(params['T0'])
		ndt=int(params['ndt'])
		icg=InitialConditionGenerator(nruns)
		gamma=float(params['gamma'])
		e=float(params['epsilon'])
		phi=float(params['phi'])
		pot=ModulatedPendulum(e,gamma,mw=-np.cos)
		tp=ContinuousTimePropagator(pot,T0=T0,ndt=ndt)
		pp=PhasePortrait(tmax,nruns,tp,icg.generateXP)
		# ~ pp=PhasePortrait(tmax,nruns,tp,icg.generateP)
		
	if potential=="KickedRotor":
		icg=InitialConditionGenerator(nruns,dP=2*np.pi)
		K=float(params['K'])
		pot=KickedRotor(K)
		tp=DiscreteTimePropagator(pot)
		pp=PhasePortrait(tmax,nruns,tp,icg.generateP)
		kicked=True	
		
	x,p=pp.computeOrbit(runid)
	
	if potential!="ModulatedPendulumRotation":
		if not(kicked):
			c=pp.getChaoticity(modc(x,2*np.pi),p)
		else:
			c=pp.getChaoticity(modc(x,2*np.pi),modc(p,2*np.pi))
	else:
		# Stockage d'information redondante 
		ncheck=int(params['ncheck'])
		c=np.zeros(tmax)
		for i in range(ncheck):
			ind=(np.arange(tmax)%ncheck)==i
			c[ind]=pp.getChaoticity(modc(x[ind],2*np.pi),p[ind])
		

	# x saved [2pi]
	if not(kicked):
		np.savez(wdir+"dataruns/"+str(runid),"w", x=modc(x,2*np.pi),p=p,c=c)
	else:
		np.savez(wdir+"dataruns/"+str(runid),"w", x=modc(x,2*np.pi),p=modc(p,2*np.pi),c=c)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	nruns=int(params['nruns'])
	tmax=int(params['tmax'])
	
	c=np.zeros((nruns,tmax))
	x=np.zeros((nruns,tmax))
	p=np.zeros((nruns,tmax))

	for i in range(nruns):
		data=np.load(wdir+"dataruns/"+str(i)+".npz")
		x[i]=data['x']
		p[i] = data['p']
		c[i]= data['c']*np.ones(tmax)
		data.close()
			
	np.savez(wdir+"data","w", x=x, p=p,c=c/np.max(c))
	os.system("rm -r "+wdir+"dataruns/")

	
if mode=="plothusimi":
	# Loading inpute file
	params=readInput(wdir+"params.txt")
	potential=params['potential']
	
	if potential=="KickedRotor":
		fig, ax = plt.subplots(figsize=(np.pi*2,np.pi*2),frameon=False)
		ax.set_ylim(-np.pi,np.pi)
	else:
		fig, ax = plt.subplots(figsize=(np.pi*2,4),frameon=False)
		ax.set_ylim(-2.0,2.0)
	
	ax.set_xlim(-np.pi,np.pi)
	
	ax.set_xticks([])
	ax.set_yticks([])
	
	data=np.load(wdir+"data.npz")
	x=data["x"]
	p=data["p"]
	c=data["c"]
	data.close()
	
	
	
	ax.scatter(x,p,s=0.5**2,c="black",lw=0.0)
	
	#condreg=c<1
	#condchaotic=c>1

	#plt.scatter(x[condchaotic],p[condchaotic],s=0.05**2,c="red",alpha=0.5)
	#plt.scatter(x[condreg],p[condreg],s=0.5**2,c="blue",lw=0.0)
	
	fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
	# ~ plt.savefig(wdir+"pp-husimi.png",dpi=250)
	plt.savefig(wdir+"pp-husimi.png",dpi=250)
	
if mode=="plot":
	# Loading inpute fil
	data=np.load(wdir+"data.npz")
	x=data["x"]
	p=data["p"]
	c=data["c"]
	data.close()
	
	fig, ax = plt.subplots(figsize=(np.pi*2,4))
	
	ax=plt.gca()
	ax.set_xlim(-np.pi,np.pi)
	ax.set_ylim(-2,2)
	ax.set_xlabel(r"Position")
	ax.set_ylabel(r"Vitesse")

	condreg=c<0.5	
	condchaotic=c>0.5

	plt.scatter(x[condchaotic],p[condchaotic],s=0.5**2,c="red",lw=0.0)
	plt.scatter(x[condreg],p[condreg],s=0.5**2,c="blue",lw=0.0)
	
	# ~ plt.scatter(x,p,s=0.02**2,c="black")
	
	plt.savefig(wdir+"pp.png",dpi=500)
	
	
if mode=="movie":
	# Loading inpute file
	
	if not os.path.isdir(wdir+"movie"): 
		os.mkdir(wdir+"movie")

	data=np.load(wdir+"data.npz")
	x=data["x"]
	p=data["p"]
	c=data["c"]
	data.close()
	
	params=readInput(wdir+"params.txt")
	ncheck=int(params['ncheck'])
	tmax=int(params['tmax'])
	nruns=int(params['nruns'])

	for i in range(ncheck):
		print(i)
		ind=((np.arange(tmax)%ncheck)==i)

		
		
		
		
		plt.clf()
		# ~ fig=getfiglatex(wf=0.81,hf=2/3.14)
		fig, ax = plt.subplots(figsize=(np.pi*2,4),frameon=False)
		
		ax.set_ylim(-2.0,2.0)

		ax.set_xlim(-np.pi,np.pi)

		ax.set_xticks([])
		ax.set_yticks([])
		# ~ ax=plt.gca()
		# ~ ax.set_xlim(-np.pi,np.pi)
		# ~ ax.set_ylim(-2,2)
		# ~ ax.set_aspect("equal")
		# ~ ax.set_xticks([-np.pi,-0.5*np.pi,0,0.5*np.pi,np.pi])
		# ~ ax.set_xticklabels([r"$-\pi$",r"$-\pi/2$",r"$0$",r"$\pi/2$",r"$\pi$"])
		# ~ ax.set_yticks([-2,-1,0,1,2])

		
		# ~ ax.set_xlabel(r"Position $x$")
		# ~ ax.set_ylabel(r"Momentum $p$")
		
		for j in range(nruns):
			# ~ print(j)
			if c[j,i]>0.5:
				plt.scatter(x[j,ind],p[j,ind],s=0.05**2,c="red")
				
			else:
				plt.scatter(x[j,ind],p[j,ind],s=0.05**2,c="blue")
		
		fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
		plt.savefig(wdir+"movie/{:05d}".format(i),dpi=250)
		# ~ for j in range(1,5):
			# ~ os.system("cp "+wdir+"movie/{:05d}.png ".format(i)+wdir+"movie/{:05d}.png".format(i+j*50))
			
		
if mode=="mkmovie":
	mkmovie(wdir+"movie/",25,moviedir=wdir)
	
	
