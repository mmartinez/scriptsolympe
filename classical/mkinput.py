import sys
import numpy as np
sys.path.insert(0, '..')
from inputparams import writeInput

dfile="g0d61-e0d63"

params={}


# ~ params['gamma']=0.15
# ~ params['epsilon']=0.60
# ~ params['T0']=100*2*np.pi#/100
# ~ params['ndt']=100*int(100)
# ~ params['tmax']=int(10000/100)
# ~ params['ntraj']=10

# Usual
params['potential']="ModulatedPendulum"
params['gamma']=0.61
params['epsilon']=0.63
params['phi']=np.pi
params['T0']=2*np.pi#/100
params['ndt']=int(100)
params['tmax']=10000

# ~ params['potential']="ModulatedPendulumFuku"
# ~ params['gamma']=0.45
# ~ params['epsilon']=0.4
# ~ params['phi']=0
# ~ params['T0']=2*np.pi#/100
# ~ params['ndt']=int(100)
# ~ params['tmax']=10000

# ~ params['T0']=0.01
# ~ params['ndt']=int(1)
# ~ params['tmax']=10000

# Rotation
# ~ params['potential']="ModulatedPendulumRotation"
# ~ params['gamma']=0.25
# ~ params['epsilon']=0.05
# ~ params['phi']=0.00
# ~ params['T0']=2*np.pi/7
# ~ params['ndt']=int(100/7)
# ~ params['tmax']=10000*7
# ~ params['ncheck']=7


# Double puit pas modulé
# ~ params['potential']="DoubleWellKicked"
# ~ params['gamma']=0.3
# ~ params['tmax']=10000


# ~ params['potential']="KickedRotor"
# ~ params['K']=0.3
# ~ params['tmax']=10000

writeInput("inputs/"+dfile+".txt",params)


