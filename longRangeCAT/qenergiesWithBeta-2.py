import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *
from scipy.linalg import logm, expm

mode=sys.argv[1]
wdir=sys.argv[2]


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
	params=readInput(wdir+"params.txt")
	Npcell=int(params['Npcell'])
	potential=params['potential']
	if potential=="KickedRotor" or potential=="KickedRotor-alpha":
		h=2*np.pi/Npcell
		addParams(wdir+"params.txt",{'h':h})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	potential=params['potential']

	beta0=np.linspace(0,1,nruns,endpoint=False)
	beta0=np.sort(beta0*(beta0<=0.5)+(beta0-1)*(beta0>0.5))
	beta=beta0[runid]

	h=float(params['h'])
	gamma=float(params['gamma'])
	e=float(params['epsilon'])
	phi=float(params['phi'])
	
	
	grid=Grid(Npcell,h)
	wf=WaveFunction(grid)
	wf.setState("coherent",xpratio=2.0)
	
	
	# epsilon=0
	pot=ModulatedPendulum(0.0,gamma,phi=phi)	
	floquet=FloquetTimePropagator(grid,pot,beta=beta,T0=4*np.pi,idtmax=1000)
			
	floquet.diagonalize()
	
	overlaps=floquet.orderEigenstatesWithOverlapOn(wf)
	quasienergies=floquet.quasienergy
	
	evec0p=floquet.eigenvec[0].p
	np.savez(wdir+"dataruns/reg-"+str(runid),"w", quasienergies=quasienergies, overlaps=overlaps,evec0p=evec0p,beta=beta)
	
	evecreg=floquet.eigenvec[0]
	
	# epsilon=/=0
	pot=ModulatedPendulum(e,gamma,phi=phi)	
	floquet=FloquetTimePropagator(grid,pot,beta=beta,T0=4*np.pi,idtmax=1000)
			
	floquet.diagonalize()
	
	overlaps=floquet.orderEigenstatesWithOverlapOn(evecreg)
	quasienergies=floquet.quasienergy
	
	evec0p=floquet.eigenvec[0].p
	
	#pour gabriel
	# ~ ham=Hamiltonian(grid,M=-1j*logm(floquet.M)*h/(4*np.pi))
	# ~ qereg=np.real(evecreg%(ham%evecreg))
	qereg=-np.angle(evecreg%(floquet%evecreg))*h/(4*np.pi)
	
	
	np.savez(wdir+"dataruns/chaotic-"+str(runid),"w", quasienergies=quasienergies, overlaps=overlaps,evec0p=evec0p,beta=beta,qereg=qereg)
	
if mode=="gatherreg":
	params=readInput(wdir+"params.txt")
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	h=float(params['h'])
	
	quasienergies=np.zeros((nruns,Npcell))
	overlaps=np.zeros((nruns,Npcell),dtype=complex)
	beta=np.zeros((nruns,Npcell))
	
	Ncell=nruns
	
	grid=Grid(Ncell*Npcell,h,xmax=Ncell*2*np.pi)
	wannier=WaveFunction(grid)
	ind=np.flipud(np.arange(0,Ncell*Npcell,Ncell))
	
	for irun in range(nruns):
		# ~ beta[irun]=np.ones(Npcell)*np.sort(beta0*(beta0<=0.5)+(beta0-1)*(beta0>0.5))[irun]
		data=np.load(wdir+"dataruns/reg-"+str(irun)+".npz")
		quasienergies[irun]=data['quasienergies']
		overlaps[irun]=data['overlaps']
		beta[irun]=np.ones(Npcell)*data['beta']
		wannier.p[ind]=np.abs(data['evec0p'])
		data.close()

		ind=ind+1

	wannier.p=np.roll(wannier.p,int(Ncell/2+1))
	wannier.p=np.fft.fftshift(wannier.p*grid.phaseshift)

	wannier.normalize("p")


	np.savez(wdir+"data-reg","w", quasienergies=quasienergies, overlaps=overlaps,beta=beta,wannierx=np.abs(wannier.x),gridx=grid.x)	
if mode=="gatherchaotic":	
	params=readInput(wdir+"params.txt")
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	h=float(params['h'])
	
	quasienergies=np.zeros((nruns,Npcell))
	overlaps=np.zeros((nruns,Npcell),dtype=complex)
	beta=np.zeros((nruns,Npcell))
	qereg=np.zeros(nruns)
	
	Ncell=nruns
	
	grid=Grid(Ncell*Npcell,h,xmax=Ncell*2*np.pi)
	wannier=WaveFunction(grid)
	ind=np.flipud(np.arange(0,Ncell*Npcell,Ncell))
	
	for irun in range(nruns):
		# ~ beta[irun]=np.ones(Npcell)*np.sort(beta0*(beta0<=0.5)+(beta0-1)*(beta0>0.5))[irun]
		data=np.load(wdir+"dataruns/chaotic-"+str(irun)+".npz")
		quasienergies[irun]=data['quasienergies']
		overlaps[irun]=data['overlaps']
		beta[irun]=np.ones(Npcell)*data['beta']
		wannier.p[ind]=np.abs(data['evec0p'])
		qereg[irun]=data['qereg']
		data.close()

		ind=ind+1

	wannier.p=np.roll(wannier.p,int(Ncell/2+1))
	wannier.p=np.fft.fftshift(wannier.p*grid.phaseshift)

	wannier.normalize("p")


	np.savez(wdir+"data-chaotic","w", quasienergies=quasienergies, overlaps=overlaps,beta=beta,wannierx=np.abs(wannier.x),gridx=grid.x,qereg=qereg)	
	
	
	os.system("rm -r "+wdir+"dataruns/")
	
if mode=="plot":
	
	data=np.load(wdir+"data-chaotic.npz")
	quasienergieschaotic=data['quasienergies']
	overlapschaotic=data['overlaps']
	qereg=data['qereg']
	beta=data['beta']
	gridx=data['gridx']
	wannierxchaotic=data['wannierx']
	
	data=np.load(wdir+"data-reg.npz")
	quasienergiesreg=data['quasienergies']
	overlapsreg=data['overlaps']
	wannierxreg=data['wannierx']
	
	ax=plt.subplot(2,2,1)
	
	
	# ~ ax.set_title(r"$\varepsilon={:.3f} \quad \gamma={:.3f} \quad 1/h={:.3f} \quad N_p={:d}$".format(float(e),float(gamma),float(1/h),int(beta.size)))
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE$")
	ax.set_xlim(np.min(beta),np.max(beta))
	
	print(np.shape(qereg))
	
	#qE=np.abs(quasienergies[:,0])/np.abs(np.mean(quasienergies[:,0]))
	
	#ax.plot(beta,np.real(qereg))
	ax.plot(beta[:,0],qereg,c="blue",label=r"$\langle \beta_{reg} |\mathcal{H}_{eff} | \beta_{reg} \rangle$")
	# ~ ax.plot(beta[:,0],quasienergieschaotic[:,0],c="red",label=r"$\langle \beta_{app} |\mathcal{H}_{eff} | \beta_{app} \rangle$")
	
	
	
	#ax.plot(beta,quasienergiesreg[:,0],c="blue",label=r"$\langle \beta_\text{reg} |\mathcal{H}_\text{eff} | \beta_\text{reg} \rangle$")
	
	ax.legend()
	ax=plt.subplot(2,2,2)
	
	ax.plot(gridx,np.abs(wannierxchaotic),c="blue",label=r"$ | n_{app} \rangle$")
	ax.plot(gridx,np.abs(wannierxreg),c="red",label=r"$| n_{reg} \rangle$")
	
	ax.set_xlim(-5*np.pi,5*np.pi)
	
	
	ax.legend()

	plt.savefig(wdir+"spectrum1.png", bbox_inches='tight',dpi=250)
	
if mode=="plotqE":
	
	fig=plt.figure(figsize=(7,2))
	
	data=np.load(wdir+"data-chaotic.npz")
	quasienergies=data['quasienergies']
	overlaps=data['overlaps']
	beta=data['beta']
	gridx=data['gridx']
	wannierx=data['wannierx']
	
	ax=plt.subplot(1,2,1)
	
	
	#ax.set_title(r"$\varepsilon={:.3f} \quad \gamma={:.3f} \quad 1/h={:.3f} \quad N_p={:d}$".format(float(e),float(gamma),float(1/h),int(beta.size)))
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE/h$")
	ax.set_xlim(np.min(beta),np.max(beta))
	
	
	qE=quasienergies[:,0]
	
	qEm=np.mean(qE)
	
	ax.set_ylim(qEm+1.5*(np.min(qE)-qEm)	,qEm+1.5*(np.max(qE)-qEm)	)	
	

	ax.plot(beta[:,0],quasienergies[:,0],c="blue",zorder=2)
	#ax.scatter(beta[:,1],quasienergies[:,1],c="red",zorder=2,s=0.5**2)
	#ax.scatter(beta[:,2],quasienergies[:,2],c="orange",zorder=2,s=0.5**2)
	# ~ ax.scatter(beta[:,3],quasienergies[:,3],c="yellow",zorder=2,s=0.5**2)
	
	# ~ for i in range(20):
		# ~ ax.scatter(beta[:,i],quasienergies[:,i],zorder=2,s=0.5**2)
	
	ax=plt.subplot(1,2,2)
	
	ax.plot(gridx/(2*np.pi),np.abs(wannierx))
	
	ax.set_xlim(-2.5,2.5)
	

	plt.savefig(wdir+"spectrum2.png", bbox_inches='tight',dpi=250)	
	
