import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]

########################################################################
# Pour comparer la dynamique TB/exact sans potentiel extérieur
# Calcule pour trois valeurs de N différentes, pour voir des effets 
# de bords.
# Calcule pour chaotic et régulier.
########################################################################


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns-TB")
	os.mkdir(wdir+"figs-TB")
	os.mkdir(wdir+"dataruns-real")
	os.mkdir(wdir+"figs-real")
	
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
	params=readInput(wdir+"params.txt")
	dirspectrum=params['dirspectrum']
	tmax=int(params['tmax'])
	tcheck=int(params['tcheck'])
	
	params=readInput(dirspectrum+"-N1079/params.txt")
	e=float(params['epsilon'])
	gamma=float(params['gamma'])
	h=float(params['h'])
	Npcell=int(params['Npcell'])
	addParams(wdir+"params.txt",{'epsilon':e})
	addParams(wdir+"params.txt",{'gamma':gamma})
	addParams(wdir+"params.txt",{'Npcell':Npcell})
	addParams(wdir+"params.txt",{'h':h})
	addParams(wdir+"params.txt",{'ncheck':int(tmax/tcheck)})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	dirspectrum=params['dirspectrum']
	nruns=int(params['nruns'])
	e=float(params['epsilon'])
	gamma=float(params['gamma'])
	h=float(params['h'])
	Npcell=int(params['Npcell'])

	tmax=int(params['tmax'])
	tcheck=int(params['tcheck'])
	ncheck=int(params['ncheck'])
	
	Ncell=269
	if runid==0 or runid==1:
		Ncell=1079
	if runid==2 or runid==3:
		Ncell=539
	if runid==4 or runid==5:
		Ncell=269	
		
	dirspectrum=dirspectrum+"-N"+str(Ncell)
	
	# 1. dyn TB
	grid=Grid(Ncell,h,xmax=Ncell)
	if runid%2==0:
		ham=Hamiltonian(grid,spectrumfile=dirspectrum+"/data-reg.npz")
		#ham=Hamiltonian(grid,spectrumfilenn=dirspectrumchaotic+"/data.npz")
	else:
		ham=Hamiltonian(grid,spectrumfile=dirspectrum+"/data-chaotic.npz")
		
	# ~ # N is odd eg: 5 => i = 0,1,[2],3,4 and (5-1)/2 =2
	i0=int((Ncell-1)/2)
	wf=WaveFunction(grid)
	wf.setState('diracx',i0=i0)
		
	tp=TimePropagator(grid,ham,dt=4*np.pi)

	prob=np.zeros((ncheck,Ncell))
	xm=np.zeros(ncheck)
	xstd=np.zeros(ncheck)
	time=np.zeros(ncheck)

	for it in range(tmax):
		wf=tp%wf # Propagation

		if it%tcheck==0:
			ind=int(it/tcheck)
			prob[ind]=np.abs(wf.x)**2
			# ~ prob[ind]/=np.max(prob[ind])
			xm[ind]=np.sum(grid.x*np.abs(wf.x)**2)
			xstd[ind]=np.sqrt(np.sum((grid.x-xm[ind])**2*np.abs(wf.x)**2))
			time[ind]=it

	
	np.savez(wdir+"dataruns-TB/N"+str(Ncell)+"-"+str(runid),prob=prob,time=time,xm=xm,xstd=xstd)	
		
	ax=plt.gca()
	plt.clf()
	ax=plt.subplot(2,1,1)
	times,n=np.meshgrid(time,grid.x)
	prob=np.swapaxes(prob,0,1)
	
	levels = np.logspace(-8,-2,35,endpoint=True)
	
	im=ax.contourf(times,n,prob, levels=levels, cmap='gnuplot2')
	
	ax=plt.subplot(2,1,2)
	ax.plot(time,xstd)
	ax.set_xlim(np.min(time),np.max(time))
	plt.savefig(wdir+"figs-TB/N"+str(Ncell)+"-"+str(runid)+".png",dpi=50)
	
	# 2. dyn real
	
	if runid%2==0:
		pot=ModulatedPendulum(0.0,gamma)	
	else:
		pot=ModulatedPendulum(e,gamma)

	grid=Grid(Npcell*Ncell,h,Ncell*2*np.pi)
	floquet=FloquetTimePropagator(grid,pot,T0=4*np.pi,idtmax=1000)
	
	gridtmp=Grid(Npcell,h)
	floquettmp=FloquetTimePropagator(gridtmp,pot,T0=4*np.pi,idtmax=1000)
	floquettmp.diagonalize()
	wftmp=WaveFunction(gridtmp)
	wftmp.setState("coherent",xpratio=2.0)
	floquettmp.orderEigenstatesWithOverlapOn(wftmp)
	
	wf0=WaveFunction(grid)
	wf0.x[np.where(np.abs(grid.x)<np.pi)]=floquettmp.eigenvec[0].x
	wf0.normalize("x")
	
	# ~ data=np.load(dirspectrum+"/data-reg.npz")
	# ~ wannierx=data['wannierx']
	# ~ data.close()
	# ~ wf0=WaveFunction(grid)
	# ~ wf0.x=np.abs(wannierx)
	# ~ wf0.normalize("x")
	
	wfcell=[]
	for i in range(Ncell):
		wfcell.insert(i,WaveFunction(grid))
		wfcell[i].x=np.roll(wf0.x,(i-int(0.5*(Ncell-1)))*Npcell)
		
	# ~ # N is odd eg: 5 => i = 0,1,[2],3,4 and (5-1)/2 =2
	i0=int((Ncell-1)/2)
	wf=WaveFunction(grid)
	wf.x=wf0.x
	wf.normalize("x")
	
	prob=np.zeros((ncheck,Ncell))
	probReal=np.zeros((ncheck,Ncell))
	probReg=np.zeros(ncheck)
	
	xm=np.zeros(ncheck)
	xstd=np.zeros(ncheck)
	xmloc=np.zeros(ncheck)
	xstdloc=np.zeros(ncheck)
	
	time=np.zeros(ncheck)
	
	for it in range(tmax):
		wf=floquet%wf
		
		if it%tcheck==0:
			ind=int(it/tcheck)
			prob[ind]=np.array([wf//wfi for wfi in wfcell])
			probReg[ind]=np.sum(prob[ind])
			prob[ind]=prob[ind]/probReg[ind]
			
			probReal[ind]=np.array([np.sum(np.abs(wf.x[i*Npcell:i*Npcell+Npcell])**2) for i in range(Ncell)])
			
			xm[ind]=np.sum(grid.x*np.abs(wf.x)**2/(2*np.pi))
			xstd[ind]=np.sqrt(np.sum((grid.x/(2*np.pi)-xm[ind])**2*np.abs(wf.x)**2))
			
			gridn=np.arange(Ncell)-(Ncell-1)/2
			xmloc[ind]=np.sum(gridn*prob[ind])
			xstdloc[ind]=np.sqrt(np.sum((gridn-xmloc[ind])**2*prob[ind]))
			time[ind]=it	
			
			
			# Renormalisation pour meilleur contraste
			# ~ prob[ind]/=np.max(prob[ind])
			# ~ probReal[ind]/=np.max(probReal[ind])

	np.savez(wdir+"dataruns-real/N"+str(Ncell)+"-"+str(runid),
												prob=prob,
												time=time,
												xm=xm,
												xstd=xstd,
												xmloc=xmloc,
												xstdloc=xstdloc,
												probReg=probReg,
												probReal=probReal)
			
	ax=plt.gca()
	plt.clf()
	ax=plt.subplot(2,1,1)
	times,n=np.meshgrid(time,np.arange(Ncell)-int(0.5*(Ncell-1)))
	prob=np.swapaxes(probReal,0,1)
	
	levels = np.logspace(-8,-2,35,endpoint=True)
	
	im=ax.contourf(times,n,prob, levels=levels, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	ax=plt.subplot(2,1,2)
	ax.plot(time,xstd)
	ax.plot(time,xstdloc)
	ax.set_xlim(np.min(time),np.max(time))
	
	
	plt.savefig(wdir+"figs-real/N"+str(Ncell)+"-"+str(runid)+".png",dpi=50)
