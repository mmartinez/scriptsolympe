import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

from matplotlib import cm

mode=sys.argv[1]
wdir=sys.argv[2]


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
	params=readInput(wdir+"params.txt")
	Npcell=int(params['Npcell'])
	potential=params['potential']
	if potential=="KickedRotor" or potential=="KickedRotor-alpha":
		h=2*np.pi/Npcell
		addParams(wdir+"params.txt",{'h':h})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	potential=params['potential']

	beta0=np.linspace(0,1,nruns,endpoint=False)
	beta0=np.sort(beta0*(beta0<=0.5)+(beta0-1)*(beta0>0.5))
	beta=beta0[runid]
	
	# ~ beta0=np.linspace(0,1,nruns,endpoint=False)
	# ~ beta=beta0[runid]

	if potential=="ModulatedPendulum":
		h=float(params['h'])
		gamma=float(params['gamma'])
		e=float(params['epsilon'])
		phi=float(params['phi'])
		pot=ModulatedPendulum(e,gamma,phi=phi)	
		grid=Grid(Npcell,h)
		floquet=FloquetTimePropagator(grid,pot,beta=beta,T0=4*np.pi,idtmax=1000)
	if potential=="KickedRotor":
		K=float(params['K'])
		h=float(params['h'])
		pot=KickedRotor(K)
		grid=Grid(Npcell,h)
		floquet=FloquetTimePropagator(grid,pot,beta=beta,T0=1,idtmax=1)
		
	if potential=="KickedRotor-alpha":
		K=float(params['K'])
		h=float(params['h'])
		pot=KickedRotor(K)
		grid=Grid(Npcell,h)
		floquet=FloquetTimePropagator(grid,pot,alpha=beta,beta=0.25,T0=1,idtmax=1)
			
	wf=WaveFunction(grid)
	wf.setState("coherent",xpratio=2.0)
	
	floquet.diagonalize()
	
	overlaps=floquet.orderEigenstatesWithOverlapOn(wf)
	quasienergies=floquet.quasienergy
	
	pmean=np.array([np.sum(grid.p*np.abs(floquet.eigenvec[i].p)**2) for i in range(Npcell)])
	pstd=np.sqrt(np.array([np.sum((grid.p-pmean[i])**2*np.abs(floquet.eigenvec[i].p)**2) for i in range(Npcell)]))
	
	xmean=np.array([np.sum(grid.x*np.abs(floquet.eigenvec[i].x)**2) for i in range(Npcell)])
	xstd=np.sqrt(np.array([np.sum((grid.x-xmean[i])**2*np.abs(floquet.eigenvec[i].x)**2) for i in range(Npcell)]))
	
	evec0p=floquet.eigenvec[0].p
	
	np.savez(wdir+"dataruns/"+str(runid),"w", quasienergies=quasienergies, overlaps=overlaps,evec0p=evec0p,beta=beta,xstd=xstd,xmean=xmean,pstd=pstd,pmean=pmean)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	h=float(params['h'])
	
	quasienergies=np.zeros((nruns,Npcell))
	overlaps=np.zeros((nruns,Npcell),dtype=complex)
	beta=np.zeros((nruns,Npcell))
	xstd=np.zeros((nruns,Npcell))
	xmean=np.zeros((nruns,Npcell))
	pstd=np.zeros((nruns,Npcell))
	pmean=np.zeros((nruns,Npcell))
	
	
	Ncell=nruns
	
	grid=Grid(Ncell*Npcell,h,xmax=Ncell*2*np.pi)
	wannier=WaveFunction(grid)
	ind=np.flipud(np.arange(0,Ncell*Npcell,Ncell))
	
	for irun in range(nruns):
		# ~ beta[irun]=np.ones(Npcell)*np.sort(beta0*(beta0<=0.5)+(beta0-1)*(beta0>0.5))[irun]
		data=np.load(wdir+"dataruns/"+str(irun)+".npz")
		quasienergies[irun]=data['quasienergies']
		overlaps[irun]=data['overlaps']
		beta[irun]=np.ones(Npcell)*data['beta']
		wannier.p[ind]=np.abs(data['evec0p'])
		
		xstd[irun]=data['xstd']
		xmean[irun]=data['xmean']
		pstd[irun]=data['pstd']
		pmean[irun]=data['pmean']
	
	
		data.close()

		ind=ind+1

	wannier.p=np.roll(wannier.p,int(Ncell/2+1))
	wannier.p=np.fft.fftshift(wannier.p*grid.phaseshift)

	wannier.normalize("p")


	np.savez(wdir+"data","w", quasienergies=quasienergies, overlaps=overlaps,beta=beta,wannierx=np.abs(wannier.x),gridx=grid.x,xstd=xstd,xmean=xmean,pstd=pstd,pmean=pmean)
	
	os.system("rm -r "+wdir+"dataruns/")
	
if mode=="plot":
	
	data=np.load(wdir+"data.npz")
	quasienergies=data['quasienergies']
	overlaps=data['overlaps']
	beta=data['beta']
	gridx=data['gridx']
	wannierx=data['wannierx']
	
	ax=plt.subplot(2,2,1)
	
	
	# ~ ax.set_title(r"$\varepsilon={:.3f} \quad \gamma={:.3f} \quad 1/h={:.3f} \quad N_p={:d}$".format(float(e),float(gamma),float(1/h),int(beta.size)))
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE/h$")
	ax.set_xlim(np.min(beta),np.max(beta))
	
	print(np.min(np.mean(quasienergies[:,0])),1.2*np.max(np.mean(quasienergies[:,0])))
	
	
	qE=np.abs(quasienergies[:,0])/np.abs(np.mean(quasienergies[:,0]))
	
	qEm=np.mean(qE)
	
	ax.set_ylim(qEm+1.5*(np.min(qE)-qEm)	,qEm+1.5*(np.max(qE)-qEm)	)	
	
	print(qEm+1.5*(np.min(qE)-qEm),qEm+1.5*(np.max(qE)-qEm))

	ax.plot(beta[:,0],qE,c="red",zorder=2)
	
	# ~ ax.plot(beta[:,0:2],np.roll(qE,int(1079/2)+1),c="blue",zorder=2)
	
	# ~ beta0=np.linspace(0,3,1079,endpoint=False)
	# ~ ax.plot(beta0,qE,c="blue",zorder=2)
	
	ax=plt.subplot(2,2,2)
	
	ax.plot(gridx,np.abs(wannierx))
	
	ax.set_xlim(-5*np.pi,5*np.pi)
	
	
	ax=plt.subplot(2,2,3)
	ax.set_ylabel(r"$V_n$")
	ax.set_xlabel(r"$n$")
	
	Vn=np.fft.rfft(quasienergies[:,0])/beta.size
	Vn=np.delete(Vn,0)
	# ~ Vn=np.delete(Vn,0)
	ax.plot(np.arange(Vn.size)+1,np.abs(Vn),c="blue")
	ax.set_yscale("log")
	# ~ ax.set_xscale("log")
	
	ax=plt.subplot(2,2,4)
	
	ax.set_ylabel(r"$V_n$")
	ax.set_xlabel(r"$n$")
	
	Vn=np.fft.rfft(quasienergies[:,0])/beta.size
	Vn=np.delete(Vn,0)
	# ~ Vn=np.delete(Vn,0)
	N=np.log(np.arange(Vn.size)+1)
	ind=(N>2)*(N<5)
	ax.scatter(N,np.log(np.abs(Vn)),c="blue")
	
	
	
	fit = np.polyfit(N[ind],np.log(np.abs(Vn[ind])), 1)
	
	ax.plot(N,fit[0]*N+fit[1],c="red")
	alpha=-fit[0]
	print(alpha)
	
	# ~ ax.set_yscale("log")
	# ~ ax.set_xscale("log")

	# ~ plt.show()

	plt.savefig(wdir+"spectrum1.png", bbox_inches='tight',dpi=250)
	
if mode=="plotqE":
	
	fig=plt.figure(figsize=(7,2))
	
	data=np.load(wdir+"data.npz")
	quasienergies=data['quasienergies']
	overlaps=data['overlaps']
	beta=data['beta']
	gridx=data['gridx']
	wannierx=data['wannierx']
	
	ax=plt.subplot(1,2,1)
	
	
	#ax.set_title(r"$\varepsilon={:.3f} \quad \gamma={:.3f} \quad 1/h={:.3f} \quad N_p={:d}$".format(float(e),float(gamma),float(1/h),int(beta.size)))
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE/h$")
	ax.set_xlim(np.min(beta),np.max(beta))
	
	
	qE=quasienergies[:,0]
	
	qEm=np.mean(qE)
	
	#ax.set_ylim(qEm+1.5*(np.min(qE)-qEm)	,qEm+1.5*(np.max(qE)-qEm)	)	
	

	ax.scatter(beta[:,0],quasienergies[:,0],c="blue",zorder=2,s=0.5**2)
	ax.scatter(beta[:,1],quasienergies[:,1],c="red",zorder=2,s=0.5**2)
	#ax.scatter(beta[:,2],quasienergies[:,2],c="orange",zorder=2,s=0.5**2)
	# ~ ax.scatter(beta[:,3],quasienergies[:,3],c="yellow",zorder=2,s=0.5**2)
	
	# ~ for i in range(20):
		# ~ ax.scatter(beta[:,i],quasienergies[:,i],zorder=2,s=0.5**2)
	
	ax=plt.subplot(1,2,2)
	
	ax.plot(gridx/(2*np.pi),np.abs(wannierx))
	
	ax.set_xlim(-2.5,2.5)
	

	plt.savefig(wdir+"spectrum2.png", bbox_inches='tight',dpi=250)	

if mode=="plotttqE":
	
	fig=plt.figure(figsize=(7,2))
	
	data=np.load(wdir+"data.npz")
	quasienergies=data['quasienergies']
	overlaps=data['overlaps']
	beta=data['beta']
	gridx=data['gridx']
	wannierx=data['wannierx']
	
	ax=plt.subplot(1,2,1)
	
	
	#ax.set_title(r"$\varepsilon={:.3f} \quad \gamma={:.3f} \quad 1/h={:.3f} \quad N_p={:d}$".format(float(e),float(gamma),float(1/h),int(beta.size)))
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE/h$")
	ax.set_xlim(np.min(beta),np.max(beta))
	
	
	qE=quasienergies[:,0]
	
	qEm=np.mean(qE)
	
	ax.set_ylim(qEm+5*(np.min(qE)-qEm)	,qEm+5*(np.max(qE)-qEm)	)	
	

	
	# ~ ax.scatter(beta[:,1],quasienergies[:,1],c="red",zorder=2,s=0.5**2)
	#ax.scatter(beta[:,2],quasienergies[:,2],c="orange",zorder=2,s=0.5**2)
	# ~ ax.scatter(beta[:,3],quasienergies[:,3],c="yellow",zorder=2,s=0.5**2)
	
	for i in range(10):
		ax.scatter(beta[:,i],quasienergies[:,i],zorder=2,s=0.5**2,c="black")
		
	ax.plot(beta[:,0],quasienergies[:,0],c="blue",zorder=2)
	
	ax=plt.subplot(1,2,2)
	
	#ax.set_title(r"$\varepsilon={:.3f} \quad \gamma={:.3f} \quad 1/h={:.3f} \quad N_p={:d}$".format(float(e),float(gamma),float(1/h),int(beta.size)))
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE/h$")
	ax.set_xlim(np.min(beta),np.max(beta))
	
	
	qE=quasienergies[:,0]
	
	qEm=np.mean(qE)
	
	# ~ ax.scatter(beta[:,1],quasienergies[:,1],c="red",zorder=2,s=0.5**2)
	#ax.scatter(beta[:,2],quasienergies[:,2],c="orange",zorder=2,s=0.5**2)
	# ~ ax.scatter(beta[:,3],quasienergies[:,3],c="yellow",zorder=2,s=0.5**2)
	
	for i in range(15):
		ax.scatter(beta[:,i],quasienergies[:,i],zorder=2,s=0.5**2)
		
	ax.plot(beta[:,0],quasienergies[:,0],c="blue",zorder=2)

	plt.savefig(wdir+"spectrum3.png", bbox_inches='tight',dpi=250)		
	
	
if mode=="plot4":
	
	fig=plt.figure(figsize=(7,2))
	
	data=np.load(wdir+"data.npz")
	quasienergies=data['quasienergies']
	overlaps=data['overlaps']
	pstd=data['pstd']
	xstd=data['xstd']
	beta=data['beta']
	gridx=data['gridx']
	wannierx=data['wannierx']
	
	params=readInput(wdir+"params.txt")
	h=float(params['h'])
	gamma=float(params['gamma'])
	e=float(params['epsilon'])
	Npcell=int(params['Npcell'])

	fig.suptitle(r"$\gamma=$"+"{:4.3f}".format(gamma)+
				r"   $\epsilon=$"+"{:4.3f}".format(e)+
				r"   $\hbar=$"+"{:4.3f}".format(h))
	
	
	ax=plt.subplot(1,2,1)
	
	
	#ax.set_title(r"$\varepsilon={:.3f} \quad \gamma={:.3f} \quad 1/h={:.3f} \quad N_p={:d}$".format(float(e),float(gamma),float(1/h),int(beta.size)))
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE/h$")
	ax.set_xlim(np.min(beta),np.max(beta))
	
	
	qE=quasienergies[:,0]
	
	qEm=np.mean(qE)
	
	ax.set_ylim(qEm+5*(np.min(qE)-qEm)	,qEm+5*(np.max(qE)-qEm)	)	
	

	
	# ~ ax.scatter(beta[:,1],quasienergies[:,1],c="red",zorder=2,s=0.5**2)
	#ax.scatter(beta[:,2],quasienergies[:,2],c="orange",zorder=2,s=0.5**2)
	# ~ ax.scatter(beta[:,3],quasienergies[:,3],c="yellow",zorder=2,s=0.5**2)
	
	imax=15
	fcolor=pstd+xstd
	ax.scatter(beta[:538,:imax],quasienergies[:538,:imax],zorder=2,s=0.5**2,c=fcolor[:538,:imax],cmap="jet")
	ax.scatter(beta[540:,:imax],quasienergies[540:,:imax],zorder=2,s=0.5**2,c=fcolor[540:,:imax],cmap="jet")	
		
	ax.plot(beta[:,0],quasienergies[:,0],c="blue",zorder=2)
	
	ax=plt.subplot(1,2,2)
	
	#ax.set_title(r"$\varepsilon={:.3f} \quad \gamma={:.3f} \quad 1/h={:.3f} \quad N_p={:d}$".format(float(e),float(gamma),float(1/h),int(beta.size)))
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE/h$")
	ax.set_xlim(np.min(beta),np.max(beta))
	
	
	qE=quasienergies[:,0]
	
	qEm=np.mean(qE)
	
	# ~ ax.scatter(beta[:,1],quasienergies[:,1],c="red",zorder=2,s=0.5**2)
	#ax.scatter(beta[:,2],quasienergies[:,2],c="orange",zorder=2,s=0.5**2)
	# ~ ax.scatter(beta[:,3],quasienergies[:,3],c="yellow",zorder=2,s=0.5**2)
	
	# ~ for i in range(12):
		# ~ ax.scatter(beta[:,i],quasienergies[:,i],zorder=2,s=0.5**2,c=np.abs(overlaps[:,i])**2,cmap="tab10")
		# ~ ax.scatter(beta[:,i],quasienergies[:,i],zorder=2,s=0.5**2,c=np.abs(overlaps[:,i])**2,cmap="gnuplot")
	
	ax.scatter(beta[:538,:imax],quasienergies[:538,:imax],zorder=2,s=0.5**2,c=fcolor[:538,:imax],cmap="jet")
	ax.scatter(beta[540:,:imax],quasienergies[540:,:imax],zorder=2,s=0.5**2,c=fcolor[540:,:imax],cmap="jet")	
		
	ax.plot(beta[:,0],quasienergies[:,0],c="blue",zorder=2)

	plt.savefig(wdir+sys.argv[3]+".png", bbox_inches='tight',dpi=250)	
