import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns-real")
	os.mkdir(wdir+"figs-real")
	
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
	params=readInput(wdir+"params.txt")
	dirspectra=params['dirspectra']
	tmax=int(params['tmax'])
	tcheck=int(params['tcheck'])
	
	params=readInput(dirspectra+"/params.txt")
	e=float(params['epsilon'])
	gamma=float(params['gamma'])
	h=float(params['h'])
	Npcell=int(params['Npcell'])
	Ncell=int(params['nruns'])
	phi=float(params['phi'])
	
	addParams(wdir+"params.txt",{'epsilon':e})
	addParams(wdir+"params.txt",{'gamma':gamma})
	addParams(wdir+"params.txt",{'Npcell':Npcell})
	addParams(wdir+"params.txt",{'Ncell':Ncell})
	addParams(wdir+"params.txt",{'h':h})
	addParams(wdir+"params.txt",{'phi':phi})
	addParams(wdir+"params.txt",{'ncheck':int(tmax/tcheck)})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	dirspectra=params['dirspectra']
	sigma=float(params['sigma'])
	nruns=int(params['nruns'])
	e=float(params['epsilon'])
	gamma=float(params['gamma'])
	h=float(params['h'])
	Npcell=int(params['Npcell'])
	Ncell=int(params['Ncell'])
	nuC=float(params['nuC'])
	beta0=float(params['beta0'])
	phi=float(params['phi'])
	
	tmax=int(params['tmax'])
	tcheck=int(params['tcheck'])
	ncheck=int(params['ncheck'])
	
	x0=-int(np.arange(nruns)[runid]/nruns*500)*2*np.pi#valeur de beta0

	
	data=np.load(dirspectra+"/data-reg.npz")
	wannierx=data['wannierx']
	data.close()
	
	
	grid=Grid(Npcell*Ncell,h,Ncell*2*np.pi)
	pot=ModulatedPendulum(e,gamma,nuC=nuC,heff=h,phi=phi)
	floquet=FloquetTimePropagator(grid,pot,T0=4*np.pi,idtmax=1000)
	
	
	wf0=WaveFunction(grid)
	wf0.x=np.abs(wannierx)
	wf0.normalize("x")
		
	wfcell=[]
	for i in range(Ncell):
		wfcell.insert(i,WaveFunction(grid))
		wfcell[i].x=np.roll(wf0.x,(i-int(0.5*(Ncell-1)))*Npcell)
		

	wf=WaveFunction(grid)
	for i in range(Ncell):
		wf.x+=wfcell[i].x*(1-np.abs((grid.x-x0)/(sigma*2*np.pi))**4)*(np.abs(grid.x-x0)<sigma*2*np.pi)*np.exp(-1j*grid.x*beta0*2*np.pi)
	wf.normalize("x")
	
	# ~ prob=np.zeros((ncheck,Ncell))
	wfx2=np.zeros((ncheck,Npcell*Ncell))
	wfp2=np.zeros((ncheck,Npcell*Ncell))
	projWan=np.zeros((ncheck,Ncell),dtype=complex)
	
	xm=np.zeros(ncheck)
	xstd=np.zeros(ncheck)
	time=np.zeros(ncheck)
	
	for it in range(tmax):
		# ~ wf=floquet%wf
		if it%tcheck==0:
			projWan[int(it/tcheck)]=np.array([np.sum(np.conj(wf.p[i::Ncell])*wf0.p[i::Ncell]) for i in range(Ncell)])
			
			wfx2[int(it/tcheck)]=np.abs(wf.x)**2
			wfp2[int(it/tcheck)]=np.abs(np.fft.fftshift(wf.p))**2
			
			xm[int(it/tcheck)]=np.sum(grid.x*np.abs(wf.x)**2/(2*np.pi))
			xstd[int(it/tcheck)]=np.sqrt(np.sum((grid.x/(2*np.pi)-xm[int(it/tcheck)])**2*np.abs(wf.x)**2))
			time[int(it/tcheck)]=it	
			
		wf=floquet%wf
			

	np.savez(wdir+"dataruns-real/"+str(runid),projWan=projWan,time=time,xm=xm,xstd=xstd,wfx2=wfx2,wfp2=wfp2,gridx=grid.x,gridp=np.fft.fftshift(grid.p))
			
	ax=plt.gca()
	ax.set_title("beta="+str(beta0))
	plt.clf()
	
	ax=plt.subplot(2,1,1)
	times,n=np.meshgrid(time,grid.x)
	prob=np.swapaxes(wfx2,0,1)
	
	im=ax.contourf(times,n,prob, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	ax=plt.subplot(2,1,2)
	
	times,n=np.meshgrid(time,np.fft.fftshift(grid.p))
	prob=np.swapaxes(wfp2,0,1)
	
	
	im=ax.contourf(times,n,prob, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	plt.savefig(wdir+"figs-real/"+str(runid)+".png",dpi=50)

	
