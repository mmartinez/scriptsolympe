import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	potential=params['potential']

	beta0=np.linspace(0,1,nruns,endpoint=False)
	beta0=np.sort(beta0*(beta0<=0.5)+(beta0-1)*(beta0>0.5))
	beta=beta0[runid]
	

	h=float(params['h'])
	gamma=float(params['gamma'])
	e=float(params['epsilon'])
	phi=float(params['phi'])
	x0=float(params['x0'])
	pot=ModulatedPendulum(e,gamma,phi=phi)	
	grid=Grid(Npcell,h)
	floquet=FloquetTimePropagator(grid,pot,beta=beta,T0=4*np.pi,idtmax=1000)
			
	wf=WaveFunction(grid)
	wf.setState("coherent",xpratio=2.0)
	wf.shift("x",x0)
	
	floquet.diagonalize()
	
	overlaps=floquet.orderEigenstatesWithOverlapOn(wf)
	
	
	
	
	i=0
	symtest=floquet.eigenvec[i]
	symtest.shift("p",beta*h)
	sym0=symtest.isSym("x")
	
	if sym0==True:
		qEsym=floquet.quasienergy[i]
		
		
		while symtest.isSym("x")==sym0:
			i+=1
			symtest=floquet.eigenvec[i]
			symtest.shift("p",beta*h)
		qEasym=floquet.quasienergy[i]	
		
		
	else:
		qEasym=floquet.quasienergy[i]
		
		
		while symtest.isSym("x")==sym0:
			i+=1
			symtest=floquet.eigenvec[i]
			symtest.shift("p",beta*h)
			
		qEsym=floquet.quasienergy[i]	
		
	
		
	
	np.savez(wdir+"dataruns/"+str(runid),"w", qEsym=qEsym,qEasym=qEasym,beta=beta,qE=floquet.quasienergy)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	nruns=int(params['nruns'])
	Npcell=int(params['Npcell'])
	
	qEsym=np.zeros(nruns)
	qEasym=np.zeros(nruns)
	qE=np.zeros((nruns,Npcell))
	beta=np.zeros(nruns)
	
	for irun in range(nruns):
		data=np.load(wdir+"dataruns/"+str(irun)+".npz")
		qEsym[irun]=data['qEsym']
		qEasym[irun]=data['qEasym']
		qE[irun]=data['qE']
		beta[irun]=data['beta']
		data.close()

	np.savez(wdir+"data","w", qEsym=qEsym,qEasym=qEasym,beta=beta,qE=qE)
	
	os.system("rm -r "+wdir+"dataruns/")
	
	
if mode=="plot":
	
	fig=plt.figure(figsize=(7,2))
	
	data=np.load(wdir+"data.npz")
	qEsym=data['qEsym']
	qEasym=data['qEasym']
	qE=data['qE']
	beta=data['beta']
	
	ax=plt.gca()
	
	#ax.set_title(r"$\varepsilon={:.3f} \quad \gamma={:.3f} \quad 1/h={:.3f} \quad N_p={:d}$".format(float(e),float(gamma),float(1/h),int(beta.size)))
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE/h$")
	ax.set_xlim(np.min(beta),np.max(beta))
	
	
	#ax.set_ylim(qEm+1.5*(np.min(qE)-qEm)	,qEm+1.5*(np.max(qE)-qEm)	)	
	

	# ~ ax.scatter(beta,qEsym,c="blue",zorder=2,s=0.5**2)
	# ~ ax.scatter(beta,qEasym,c="red",zorder=2,s=0.5**2)
	print(np.shape(qE),np.shape(beta))
	for i in range(10):
		
		ax.scatter(beta,qE[:,i],c="black",zorder=2,s=0.5**2)
		# ~ ax.scatter(beta,qE[:,i],zorder=2,s=0.5**2)
		
	ax.scatter(beta,qEsym,c="blue",zorder=2,s=0.5**2)
	ax.scatter(beta,qEasym,c="red",zorder=2,s=0.5**2)

	plt.savefig(wdir+"spectrum.png", bbox_inches='tight',dpi=250)
	
	plt.clf()
	ax=plt.subplot(2,1,1)
	
	#ax.set_title(r"$\varepsilon={:.3f} \quad \gamma={:.3f} \quad 1/h={:.3f} \quad N_p={:d}$".format(float(e),float(gamma),float(1/h),int(beta.size)))
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE/h$")
	ax.set_xlim(np.min(beta),np.max(beta))
	
	
	qEm=np.mean(qEsym)
	
	ax.set_ylim(qEm+1.5*(np.min(qEsym)-qEm)	,qEm+1.5*(np.max(qEsym)-qEm)	)	
	

	ax.scatter(beta,qEsym,c="blue",zorder=2,s=0.5**2)
	
	ax=plt.subplot(2,1,2)
	
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE/h$")
	ax.set_xlim(np.min(beta),np.max(beta))
	
	qEm=np.mean(qEasym)
	
	ax.set_ylim(qEm+1.5*(np.min(qEasym)-qEm)	,qEm+1.5*(np.max(qEasym)-qEm)	)
	
	ax.scatter(beta,qEasym,c="red",zorder=2,s=0.5**2)


	plt.savefig(wdir+"spectrum-sep.png", bbox_inches='tight',dpi=250)
	
