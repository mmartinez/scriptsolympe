import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns-real")
	os.mkdir(wdir+"figs-real")
	
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
	params=readInput(wdir+"params.txt")
	dirspectra=params['dirspectra']
	tmax=int(params['tmax'])
	tcheck=int(params['tcheck'])
	
	params=readInput(dirspectra+"/params.txt")
	e=float(params['epsilon'])
	gamma=float(params['gamma'])
	h=float(params['h'])
	Npcell=int(params['Npcell'])
	Ncell=int(params['nruns'])
	addParams(wdir+"params.txt",{'epsilon':e})
	addParams(wdir+"params.txt",{'gamma':gamma})
	addParams(wdir+"params.txt",{'Npcell':Npcell})
	addParams(wdir+"params.txt",{'Ncell':Ncell})
	addParams(wdir+"params.txt",{'h':h})
	addParams(wdir+"params.txt",{'ncheck':int(tmax/tcheck)})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	dirspectra=params['dirspectra']
	sigma=float(params['sigma'])
	nruns=int(params['nruns'])
	e=float(params['epsilon'])
	gamma=float(params['gamma'])
	h=float(params['h'])
	Npcell=int(params['Npcell'])
	Ncell=int(params['Ncell'])
	nuC=float(params['nuC'])
	
	tmax=int(params['tmax'])
	tcheck=int(params['tcheck'])
	ncheck=int(params['ncheck'])
	
	beta0=np.linspace(0,0.5,nruns)[runid] #valeur de beta0

	# 2. dyn real
	
	data=np.load(dirspectra+"/data-reg.npz")
	wannierx=data['wannierx']
	data.close()
	
	
	grid=Grid(Npcell*Ncell,h,Ncell*2*np.pi)
	# ~ pot=ModulatedPendulum(e,gamma)
	# ~ floquet=FloquetTimePropagator(grid,pot,T0=4*np.pi,idtmax=1000)
	
	T0=4*np.pi
	ndt=1000
	dt=T0/ndt

	lqm=LoadingQM(beta0,gamma,h)
	pot=ModulatedPendulumReal(e,gamma,lqm,nuC,h)
	tp=TDTimePropagator(grid,pot,dt)
	
	t=0
	
	wf0=WaveFunction(grid)
	wf0.x=np.abs(wannierx)
	wf0.normalize("x")
		
	wfcell=[]
	for i in range(Ncell):
		wfcell.insert(i,WaveFunction(grid))
		wfcell[i].x=np.roll(wf0.x,(i-int(0.5*(Ncell-1)))*Npcell)
		

	wf=WaveFunction(grid)
	for i in range(Ncell):
		wf.x+=wfcell[i].x*(1-np.abs(grid.x/(sigma*2*np.pi))**4)*(np.abs(grid.x)<sigma*2*np.pi)
	wf.normalize("x")
	
	# ~ prob=np.zeros((ncheck,Ncell))
	probReal=np.zeros((ncheck,Ncell))
	probReg=np.zeros(ncheck)
	projWan=np.zeros((ncheck,Ncell),dtype=complex)
	
	xm=np.zeros(ncheck)
	xstd=np.zeros(ncheck)
	time=np.zeros(ncheck)
	
	

	
	while t<lqm.Tswap:
		wf,t=tp.propagate(wf,t,1)
	
		
	for it in range(tmax):
		# ~ wf=floquet%wf
		if it%tcheck==0:
			wf.shift("p",-lqm.v)	
			# ~ prob[int(it/tcheck)]=np.array([wf//wfi for wfi in wfcell])
			projWan[int(it/tcheck)]=np.array([np.sum(np.conj(wf.p[i::Ncell])*wf0.p[i::Ncell]) for i in range(Ncell)])
			wf.shift("p",lqm.v)	
			
			probReal[int(it/tcheck)]=np.array([np.sum(np.abs(wf.x[i*Npcell:i*Npcell+Npcell])**2) for i in range(Ncell)])
			
			# ~ probReg[int(it/tcheck)]=np.sum(prob[int(it/tcheck)])
			
			# ~ prob[int(it/tcheck)]/=np.max(prob[int(it/tcheck)])
			probReal[int(it/tcheck)]/=np.max(probReal[int(it/tcheck)])

			xm[int(it/tcheck)]=np.sum(grid.x*np.abs(wf.x)**2/(2*np.pi))
			xstd[int(it/tcheck)]=np.sqrt(np.sum((grid.x/(2*np.pi)-xm[int(it/tcheck)])**2*np.abs(wf.x)**2))
			time[int(it/tcheck)]=it	
			
		wf,t=tp.propagate(wf,t,ndt)
			

	np.savez(wdir+"dataruns-real/"+str(runid),projWan=projWan,time=time,xm=xm,probReg=probReg,xstd=xstd,probReal=probReal,v=lqm.v,Tswap=lqm.Tswap)
			
	ax=plt.gca()
	ax.set_title("beta="+str(beta0))
	plt.clf()
	
	ax=plt.subplot(4,1,1)
	
	ax.plot(np.abs(projWan[0])**2)
	
	
	ax=plt.subplot(4,1,2)
	times,n=np.meshgrid(time,np.arange(Ncell)-int(0.5*(Ncell-1)))
	prob=np.swapaxes(probReal,0,1)
	levels = np.linspace(0,1,10,endpoint=True)
	
	im=ax.contourf(times,n,prob, levels=levels, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	ax=plt.subplot(4,1,3)
	ax.plot(time,xm)
	ax.set_xlim(np.min(time),np.max(time))
	
	ax=plt.subplot(4,1,4)
	ax.plot(time,probReg)
	ax.set_ylim(0,1)
	ax.set_xlim(np.min(time),np.max(time))

	
	plt.savefig(wdir+"figs-real/"+str(runid)+".png",dpi=50)

	
