import sys
sys.path.insert(0, '..')
from inputparams import writeInput

dfile="testres"

params={}

params['h']=0.346
params['gamma']=0.374
params['epsilon']=0.244
params['x0']=1.72
params['beta0']=0.0


params['hmin']=1/3.5
params['hmax']=1/2.5

# ~ params['Ncell']=125
params['Npcell']=32

# ~ params['nperiods']=10000


# bif
# ~ params['epsilon']=0.268
# ~ params['Npcell']=64
# ~ params['gmin']=0.18
# ~ params['gmax']=0.36
# ~ params['hmin']=0.232
# ~ params['hmax']=0.232
# ~ params['nx0']=50
# ~ params['nh']=1
# ~ params['dtmax']=2
	

writeInput("inputs/"+dfile+".txt",params)


