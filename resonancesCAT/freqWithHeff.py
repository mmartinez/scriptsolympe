import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

from matplotlib import cm

mode=sys.argv[1]
wdir=sys.argv[2]

########################################################################
# Simule la dynamique d'un paquet d'onde placée en x0, pendant nperiods.
# La valeur moyenne de la position est calculé à chaque période.
# Puis, la TF est calculée à la fin de la simulation.
########################################################################


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	gamma=float(params['gamma'])
	e=float(params['epsilon'])
	x0=float(params['x0'])
	beta0=float(params['beta0'])
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	nperiods=int(params['nperiods'])
	
	hmin=float(params['hmin'])
	hmax=float(params['hmax'])
	
	
	h=np.linspace(hmin,hmax,nruns,endpoint=False)[runid]

	grid=Grid(Npcell,h)
	pot=ModulatedPendulum(e,gamma)
	floquet=FloquetTimePropagator(grid,pot,T0=4*np.pi,idtmax=1000,beta=beta0)

	wf=WaveFunction(grid)
	wf.setState("coherent",xpratio=2.0)
	wf.shift("x",x0)
	
	xm=np.zeros(nperiods)
	
	for it in range(nperiods):
		wf=floquet%wf
		xm[it]=np.sum(np.abs(wf.x)**2*grid.x)
		
	time=np.arange(nperiods)
	fspectrum=np.abs(np.fft.rfft(xm*np.exp(-time/1000)))
	
	np.savez(wdir+"dataruns/"+str(runid),"w", xm=xm,fspectrum=fspectrum)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	gamma=float(params['gamma'])
	e=float(params['epsilon'])
	x0=float(params['x0'])
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	nperiods=int(params['nperiods'])
	
	hmin=float(params['hmin'])
	hmax=float(params['hmax'])
	
	h=np.linspace(hmin,hmax,nruns,endpoint=False)
	fspectrum=np.zeros((nruns,int(nperiods/2)+1))
	xm=np.zeros((nruns,nperiods))
	time=np.arange(nperiods)*2
	freq=np.fft.rfftfreq(nperiods,d=2.0)
	
	for irun in range(nruns):
		data=np.load(wdir+"dataruns/"+str(irun)+".npz")
		fspectrum[irun]=data['fspectrum']
		xm[irun]=data['xm']

	np.savez(wdir+"data","w", h=h,fspectrum=fspectrum,xm=xm,time=time,freq=freq)	
	
	os.system("rm -r "+wdir+"dataruns/")
	
if mode=="plot":
	
	data=np.load(wdir+"data.npz")
	time=data['time']
	freq=data['freq']
	h=data['h']
	xm=data['xm']
	fspectrum=data['fspectrum']
	data.close()
	
	ax=plt.gca()
	ax.set_xlabel(r"$\hbar_{eff}$")
	ax.set_ylabel(r"Oscillation frequencies")
	ax.set_yscale("log")
	ax.set_ylim(freq[1],np.max(freq))
	
	hm,freq=np.meshgrid(1/h,freq)	
	plt.pcolormesh(hm,freq,np.transpose(fspectrum),cmap=plt.get_cmap('Reds'))
	

	plt.savefig(wdir+"fspectrum.png", bbox_inches='tight',dpi=250)
	
	
	
