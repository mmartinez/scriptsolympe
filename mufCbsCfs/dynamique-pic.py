import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]

if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")

	inputfile=sys.argv[3]+".txt"
	os.system("cp "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})

if mode=="compute":
	# Loading input file
	
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	potential=params['potential']
	N=int(params['N'])
	nav=int(params['nav'])
	alpha=float(params['alpha'])
	i0=int(params['i0'])
	nruns=int(params['nruns'])
	taumax=int(params['taumax'])

	grid=Grid(N,h=1,xmax=2*np.pi)
	
	if potential=="Rectangle":
		pot=Rectangle(alpha)
		
	if potential=="RectangleAsym":
		pot=Rectangle(alpha,x1=0,x2=np.pi)
	
	if potential=="SawTooth":
		pot=SawTooth(alpha)
	
	tmax=int(taumax*N/(2*np.pi))
	lambdacfs=np.zeros(tmax)

	for n in range(nav):
		fo=FloquetTimePropagator(grid,pot,T0=1,idtmax=1,randomphase=True)
		# Dynamique
		wf=WaveFunction(grid)
		wf.setState("diracx",i0=i0)
		for it in range(0,tmax):
			wf=fo%wf
			lambdacfs[it]+=np.abs(wf.x[i0])**2	
				
	lambdacfs/=nav
	
	np.savez(wdir+"dataruns/"+str(runid),lambdacfs=lambdacfs)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	nruns=int(params['nruns'])
	N=int(params['N'])
	alpha=float(params['alpha'])
	taumax=int(params['taumax'])
	nav=int(params['nav'])
	
	tmax=int(taumax*N/(2*np.pi))
	lambdacfs=np.zeros(tmax)

	for i in range(nruns):
		data=np.load(wdir+"dataruns/"+str(i)+".npz")
		lambdacfs+=data['lambdacfs']
		data.close()
		
	lambdacfs/=nruns
	
	time=np.arange(tmax)+1
	
	np.savez(wdir+"data",time=time,lambdacfs=lambdacfs*N-1)
	os.system("rm -r "+wdir+"dataruns/")

				
	


