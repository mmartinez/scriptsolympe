import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]

if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")

	inputfile=sys.argv[3]+".txt"
	os.system("cp "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})

if mode=="compute":
	# Loading input file
	
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	potential=params['potential']
	N=int(params['N'])
	i0=int(params['i0'])
	nruns=int(params['nruns'])
	nav=int(params['nav'])
	taumax=int(params['taumax'])
	
	alphat=np.array([0.049,0.191,0.322,0.446,0.564,0.686,0.861])
	palpha=np.array([1,2,4,8,16,32,64])
	palpha=np.round(palpha/np.sum(palpha)*1799)
	ialpha=np.repeat(np.arange(7),palpha.astype(int))[runid]
	
	nalpha=alphat.shape[0]
	
	alpha=alphat[ialpha]
	
	tmax=int(taumax*N/(2*np.pi))
	lambdacfs=np.zeros(tmax)
	
	for iav in range(nav):
		grid=Grid(N,h=1,xmax=2*np.pi)
		pot=SawTooth(alpha)
		fo=FloquetTimePropagator(grid,pot,T0=1,idtmax=1,randomphase=True)
		# Dynamique
		wf=WaveFunction(grid)
		wf.setState("diracx",i0=i0)
		for it in range(0,tmax):
			wf=fo%wf
			lambdacfs[it]+=np.abs(wf.x[i0])**2	
	
	np.savez(wdir+"dataruns/"+str(runid),lambdacfs=lambdacfs*N/nav-1)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	nruns=int(params['nruns'])
	N=int(params['N'])
	taumax=int(params['taumax'])
	
	
	alphat=np.array([0.049,0.191,0.322,0.446,0.564,0.686,0.861])
	nalpha=alphat.shape[0]

	
	tmax=int(taumax*N/(2*np.pi))
	lambdacfs=np.zeros((nalpha,tmax))
	
	palpha=np.array([1,2,4,8,16,32,64])
	palpha=np.round(palpha/np.sum(palpha)*1799)
	palphat=np.repeat(np.arange(7),palpha.astype(int))
	
	
	for i in range(nruns):
		ialpha=palphat[i]
		data=np.load(wdir+"dataruns/"+str(i)+".npz")
		lambdacfs[ialpha]+=data['lambdacfs']
		data.close()
		
	
	lambdacfs=lambdacfs/palpha[:,np.newaxis]

	
	time=np.arange(tmax)+1
	
	np.savez(wdir+"data",time=time,lambdacfs=lambdacfs)
	os.system("rm -r "+wdir+"dataruns/")

				
	


