import sys
import numpy as np

sys.path.insert(0, '..')
from inputparams import writeInput



params={}
# ~ params['nDq']=25

# 1.Nombre de valeurs du paramètre varié pour calculer les D2
# ~ dfile="sawTooth"
# ~ params['potential']="Rectangle"
# ~ params['potential']="SawTooth"

# ~ # 2a. Pour propagation temporelle
# ~ dfile="a0d322-N1024"
# ~ params['potential']="SawTooth"
# ~ params['N']=1024
# ~ params['nav']=1
# ~ params['i0']=int(params['N']/4)
# ~ params['taumax']=200
# ~ params['alpha']=0.322

# ~ # 2b. Pour propagation temporelle totale (forme)
# ~ dfile="a0d322-N65536"
# ~ params['potential']="SawTooth"
# ~ params['N']=65536
# ~ params['nav']=1
# ~ params['i0']=int(params['N']/4)
# ~ params['tmax']=200000
# ~ params['alpha']=0.322
# ~ params['tcheck']=500

# ~ # 2c. Pour propagation temporelle avec a
# ~ dfile="N32768"
# ~ params['potential']="SawTooth"
# ~ params['N']=32768
# ~ params['nav']=625
# ~ params['i0']=int(params['N']/4)
# ~ params['taumax']=1

# 4. b_omega
# ~ dfile="N4096-a0d322"
# ~ params['potential']="SawTooth"
# ~ params['N']=4096
# ~ params['nav']=1
# ~ params['i0']=int(params['N']/4)
# ~ params['nomega']=25
# ~ params['alpha']=0.322

# 4. forme-pic
# ~ dfile="a0d861-N2048"
# ~ params['potential']="SawTooth"
# ~ params['alpha']=0.861
# ~ params['N']=2048
# ~ params['nav']=4
# ~ params['i0']=int(params['N']/4)

# 5. contraste/forme avec a
dfile="N128-nav256"
params['potential']="SawTooth"
params['N']=128
params['nav']=256
params['i0']=int(params['N']/4)

# 6. D1D2
# ~ dfile="test"
# ~ params['potential']="SawTooth"

writeInput("inputs/"+dfile+".txt",params)


