import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]

if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")

	inputfile=sys.argv[3]+".txt"
	os.system("cp "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})

if mode=="compute":
	# Loading input file
	
	runid=int(sys.argv[3])-1
	params=readInput(wdir+"params.txt")
	potential=params['potential']
	nruns=int(params['nruns'])	
	
	alphat=np.array([0.049,0.191,0.322,0.446,0.564,0.686,0.861])
	nalpha=alphat.shape[0]
	
	Nt=np.array([256,512,1024,2048,4096])
	nN=Nt.shape[0]
	nav=np.array([16,8,4,2,1])
	
	moment2d00=np.zeros((nN,nalpha))
	moment0d95=np.zeros((nN,nalpha))
	moment1d05=np.zeros((nN,nalpha))
	
	for iN,N in enumerate(Nt):
		for iav in range(nav[iN]):
			for ialpha,alpha in enumerate(alphat):
				grid=Grid(N,h=1,xmax=2*np.pi)
				pot=SawTooth(alpha)
				
				fo=FloquetTimePropagator(grid,pot,T0=1,idtmax=1,randomphase=True)
				fo.diagonalize()
				
				for ievec in range(N):
					moment2d00[iN,ialpha]+=np.sum(np.abs(fo.eigenvec[ievec].p)**4)
					moment0d95[iN,ialpha]+=np.sum(np.abs(fo.eigenvec[ievec].p)**(2*0.95))
					moment1d05[iN,ialpha]+=np.sum(np.abs(fo.eigenvec[ievec].p)**(2*1.05))
					
		moment2d00[iN]/=(nav[iN]*N) #moyenne sur les vecteurs propres -> /N
		moment0d95[iN]/=(nav[iN]*N)	
		moment1d05[iN]/=(nav[iN]*N)
	
	np.savez(wdir+"dataruns/"+str(runid),moment2d00=moment2d00,moment0d95=moment0d95,moment1d05=moment1d05)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	nruns=int(params['nruns'])	
		
	alphat=np.array([0.049,0.191,0.322,0.446,0.564,0.686,0.861])
	nalpha=alphat.shape[0]
	
	Nt=np.array([256,512,1024,2048,4096])
	nN=Nt.shape[0]
	
	moment2d00=np.zeros((nN,nalpha))
	moment0d95=np.zeros((nN,nalpha))
	moment1d05=np.zeros((nN,nalpha))
	
	for i in range(nruns):
		data=np.load(wdir+"dataruns/"+str(i)+".npz")
		moment2d00+=data['moment2d00']
		moment0d95+=data['moment0d95']
		moment1d05+=data['moment1d05']

	moment2d00/=nruns
	moment0d95/=nruns		
	moment1d05/=nruns

	np.savez(wdir+"data",
		moment2d00=moment2d00,
		moment0d95=moment0d95,
		moment1d05=moment1d05,
		N=Nt,
		alpha=alphat)
		
	# ~ os.system("rm -r "+wdir+"dataruns/")

if mode=="plot":
	params=readInput(wdir+"params.txt")
	N=int(params['N'])
	
	data=np.load(wdir+"data.npz")
	moment2d000=data['moment2d000']
	moment0d999=data['moment0d999']
	moment1d001=data['moment1d001']
	N=data['N']
	alpha=data['alpha']
	data.close()
	
	ax=plt.subplot(2,1,1)
	for ialpha in range(len(alpha)):
		ax.scatter(N,moment2d000[:,ialpha])
	#print(np.mean(lambdainf))
	
	ax=plt.subplot(2,1,2)
	for ialpha in range(len(alpha)):
		ax.scatter(N,moment1d001[:,ialpha])

	plt.savefig(wdir+"forme.png", bbox_inches = 'tight',format="png")

				
	


