import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]

if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")

	inputfile=sys.argv[3]+".txt"
	os.system("cp "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})

if mode=="compute":
	# Loading input file
	
	runid=int(sys.argv[3])-1
	params=readInput(wdir+"params.txt")
	potential=params['potential']
	N=int(params['N'])
	nav=int(params['nav'])
	i0=int(params['i0'])
	nruns=int(params['nruns'])	
	
	alphat=np.array([0.049,0.191,0.322,0.446,0.564,0.686,0.861])
	nalpha=alphat.shape[0]

		
	lambdainf=np.zeros((nalpha,N))

	for iav in range(nav):
		for ialpha,alpha in enumerate(alphat):
			grid=Grid(N,h=1,xmax=2*np.pi)
			pot=SawTooth(alpha)
			
			fo=FloquetTimePropagator(grid,pot,T0=1,idtmax=1,randomphase=True)
			fo.diagonalize()
			
			for ix in range(N):
				lambdainf[ialpha,ix]+=N*np.sum(np.array([np.sum(np.abs(fo.eigenvec[ievec].x[i0])**2*np.abs(fo.eigenvec[ievec].x[ix])**2) for ievec in range(N)]))

	
	np.savez(wdir+"dataruns/"+str(runid),lambdainf=lambdainf/nav-1)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	N=int(params['N'])
	nav=int(params['nav'])
	nruns=int(params['nruns'])	
	
	alphat=np.array([0.049,0.191,0.322,0.446,0.564,0.686,0.861])
	nalpha=alphat.shape[0]

	lambdainf=np.zeros((nalpha,N))
	lambdainf_mean=np.zeros((nalpha,N))
	lambdainf_std=np.zeros((nalpha,N))
	
	for i in range(nruns):
		data=np.load(wdir+"dataruns/"+str(i)+".npz")
		lambdainf+=data['lambdainf']
		data.close()

	lambdainf=lambdainf/nruns
	
	np.savez(wdir+"data",
		lambdainf=lambdainf,
		alpha=alphat)
	#os.system("rm -r "+wdir+"dataruns/")
	
if mode=="gather2spec":
	params=readInput(wdir+"params.txt")
	N=int(params['N'])
	nav=int(params['nav'])
	nruns=int(params['nruns'])	
	
	alphat=np.array([0.049,0.191,0.322,0.446,0.564,0.686,0.861])
	nalpha=alphat.shape[0]

	lambdainf=np.zeros((nalpha,N))

	for i in [1,2]:
		data=np.load(wdir+"data-"+str(i)+".npz")
		lambdainf+=data['lambdainf']
		data.close()

	lambdainf=lambdainf/2
	
	np.savez(wdir+"data",
		lambdainf=lambdainf,
		alpha=alphat)
	os.system("rm -r "+wdir+"dataruns/")

if mode=="plot":
	params=readInput(wdir+"params.txt")
	N=int(params['N'])
	
	data=np.load(wdir+"data.npz")
	lambdainf=data['lambdainf']
	alpha=data['alpha']
	data.close()
	
	ax=plt.gca()

	ax.plot(alpha,lambdainf[:,3])
	#print(np.mean(lambdainf))

	plt.savefig(wdir+"forme.png", bbox_inches = 'tight',format="png")

				
	


