import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]

if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")

	inputfile=sys.argv[3]+".txt"
	os.system("cp "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})

if mode=="compute":
	# Loading input file
	
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	potential=params['potential']
	N=int(params['N'])
	nav=int(params['nav'])
	nruns=int(params['nruns'])	
	nomega=int(params['nomega'])
	alpha=float(params['alpha'])
	i0=int(params['i0'])

	grid=Grid(N,h=1,xmax=2*np.pi)
	

	corrnumx=np.zeros(nomega)
	corrnump=np.zeros(nomega)
	corrdenum=np.zeros(nomega)
	
	omega=np.logspace(np.log10(2*np.pi*alpha/N),np.log10(np.pi),nomega)

	for iav in range(nav):
		pot=SawTooth(alpha)
		fo=FloquetTimePropagator(grid,pot,T0=1,idtmax=1,randomphase=True)
		fo.diagonalize()
	
		for i in range(N):
			for j in range(0,i):
				omegaij=np.abs(fo.diffQuasienergy(fo.quasienergy[i],fo.quasienergy[j]))
				ind=np.where(omega>=omegaij)[0][0]
				
				corrnumx[ind]+=np.abs(fo.eigenvec[i].x[i0])**2*np.abs(fo.eigenvec[j].x[i0])**2
				corrnump[ind]+=np.sum(np.abs(fo.eigenvec[i].p)**2*np.abs(fo.eigenvec[j].p)**2)
				corrdenum[ind]+=1

	np.savez(wdir+"dataruns/"+str(runid),
		corrnumx=corrnumx/nav,
		corrnump=corrnump/N/nav,
		corrdenum=corrdenum/nav)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	nruns=int(params['nruns'])
	N=int(params['N'])
	nomega=int(params['nomega'])
	nav=int(params['nav'])
	alpha=float(params['alpha'])
	
	omega=np.logspace(np.log10(2*np.pi*alpha/N),np.log10(np.pi),nomega)
	
	corrnumx=np.zeros(nomega)
	corrnump=np.zeros(nomega)
	corrdenum=np.zeros(nomega)
	
	
	for i in range(nruns):
		data=np.load(wdir+"dataruns/"+str(i)+".npz")
		corrnumx+=data['corrnumx']
		corrnump+=data['corrnump']
		corrdenum+=data['corrdenum']
		data.close()
		

	corrnumx/=nruns
	corrnump/=nruns
	corrdenum/=nruns
	
	corrx=N*corrnumx/(corrdenum*1/N)
	corrp=corrnump/(corrdenum*1/N)
	
	np.savez(wdir+"data",
		corrx=corrx,
		corrp=corrp,
		corrdenum=corrdenum,
		omega=omega)
	
	
	os.system("rm -r "+wdir+"dataruns/")

if mode=="plot":
	params=readInput(wdir+"params.txt")
	nomega=int(params['nomega'])
	alpha=float(params['alpha'])
	N=int(params['N'])
	
	data=np.load(wdir+"data.npz")
	bxlog=data['bxlog']
	bplog=data['bplog']
	bdenumlog=data['bdenumlog']
	omegalog=data['omegalog']
	bxlin=data['bxlin']
	bplin=data['bplin']
	bdenumlin=data['bdenumlin']
	omegalin=data['omegalin']
	data.close()
	
	ax=plt.gca()

	ax.set_xlabel(r"$\omega/\Delta$")
	ax.set_ylabel(r"N*b")

	
	ax.set_xscale('log')
	ax.set_yscale('log')
	#ax.scatter(omegalog,1-N*bxlog)
	ax.scatter(omegalog,bplog)
	# ~ ax.plot(omegalog,(omegalog)**(alpha-1))
	# ~ ax.plot(omegalog,(omegalog)**(-2),c="black")

	plt.savefig(wdir+"bp.png", bbox_inches = 'tight',format="png")
	plt.clf()
	ax=plt.gca()
	
	

	ax.set_xlabel(r"$\omega/\Delta$")
	ax.set_ylabel(r"N*b")

	ax.set_yscale('log')
	ax.set_xscale('log')
	#ax.scatter(omegalog,1-N*bxlog)
	ax.scatter(omegalin,bplin)
	# ~ ax.plot(omegalog,(omegalog)**(alpha-1))
	# ~ ax.plot(omegalog,(omegalog)**(-2),c="black")

	plt.savefig(wdir+"bp-lin.png", bbox_inches = 'tight',format="png")

				
	


